use serde::Serialize;

pub use self::{edit_message::*, send_message::*};

pub enum ParseMode {
    None,
    Markdown,
    HTML,
}

mod send_message {
    use serde::Serialize;

    use super::ParseMode;
    use crate::api::ReplayMarkup;

    /// `/sendMessage`
    ///
    /// [API Doc](https://core.telegram.org/bots/api#sendmessage)
    ///
    /// The response is of type [`MessageResponse`](../api/type.MessageResponse.html)
    #[derive(Serialize, Debug)]
    #[cfg_attr(test, derive(PartialEq))]
    pub struct SendMessage<T: ReplayMarkup> {
        chat_id: i32,
        text: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        disable_web_page_preview: Option<bool>,
        #[serde(skip_serializing_if = "Option::is_none")]
        disable_notification: Option<bool>,
        #[serde(skip_serializing_if = "Option::is_none")]
        reply_to_message_id: Option<i32>,
        #[serde(skip_serializing_if = "Option::is_none")]
        reply_markup: Option<T>,
    }

    pub struct SendMessageBuilder<T: ReplayMarkup> {
        inner: SendMessage<T>,
    }

    impl<T> SendMessageBuilder<T>
    where
        T: ReplayMarkup,
    {
        pub fn parse_mode(mut self, mode: &ParseMode) -> Self {
            self.inner.parse_mode = match *mode {
                ParseMode::None => None,
                ParseMode::Markdown => Some("markdown".to_owned()),
                ParseMode::HTML => Some("html".to_owned()),
            };

            self
        }

        pub fn disable_web_page_preview(mut self) -> Self {
            self.inner.disable_web_page_preview = Some(true);

            self
        }

        pub fn disable_notification(mut self) -> Self {
            self.inner.disable_notification = Some(true);

            self
        }

        pub fn reply_to_message_id(mut self, to: i32) -> Self {
            self.inner.reply_to_message_id = Some(to);

            self
        }

        pub fn reply_markup(mut self, markup: T) -> Self {
            self.inner.reply_markup = Some(markup);

            self
        }

        pub fn build(self) -> SendMessage<T> {
            self.inner
        }
    }

    impl<T> SendMessage<T>
    where
        T: ReplayMarkup,
    {
        pub fn builder(chat_id: i32, text: &str) -> SendMessageBuilder<T> {
            SendMessageBuilder {
                inner: SendMessage {
                    chat_id,
                    text: text.to_owned(),
                    parse_mode: None,
                    disable_web_page_preview: None,
                    disable_notification: None,
                    reply_to_message_id: None,
                    reply_markup: None,
                },
            }
        }
    }

    impl SendMessage<()> {
        pub fn new(chat_id: i32, text: &str) -> Self {
            SendMessage {
                chat_id,
                text: text.to_owned(),
                parse_mode: None,
                disable_web_page_preview: None,
                disable_notification: None,
                reply_to_message_id: None,
                reply_markup: None,
            }
        }

        pub fn reply(chat_id: i32, text: &str, reply_to_message_id: i32) -> Self {
            SendMessage {
                chat_id,
                text: text.to_owned(),
                parse_mode: None,
                disable_web_page_preview: None,
                disable_notification: None,
                reply_to_message_id: Some(reply_to_message_id),
                reply_markup: None,
            }
        }

        pub fn markdown(chat_id: i32, text: &str) -> Self {
            SendMessage {
                chat_id,
                text: text.to_owned(),
                parse_mode: Some("markdown".to_owned()),
                disable_web_page_preview: None,
                disable_notification: None,
                reply_to_message_id: None,
                reply_markup: None,
            }
        }

        pub fn html(chat_id: i32, text: &str) -> Self {
            SendMessage {
                chat_id,
                text: text.to_owned(),
                parse_mode: Some("html".to_owned()),
                disable_web_page_preview: None,
                disable_notification: None,
                reply_to_message_id: None,
                reply_markup: None,
            }
        }
    }

    #[test]
    pub fn test_builder_simple() {
        assert_eq!(
            SendMessage::builder(3, "text").build(),
            SendMessage::new(3, "text")
        );
        assert_eq!(
            SendMessage::builder(3, "text")
                .parse_mode(&ParseMode::None)
                .build(),
            SendMessage::new(3, "text")
        );
        assert_eq!(
            SendMessage::builder(3, "text")
                .parse_mode(&ParseMode::Markdown)
                .build(),
            SendMessage::markdown(3, "text")
        );
        assert_eq!(
            SendMessage::builder(3, "text")
                .parse_mode(&ParseMode::HTML)
                .build(),
            SendMessage::html(3, "text")
        );
    }

    #[test]
    pub fn test_builder_all() {
        use crate::api::{InlineKeyboardButton, InlineKeyboardMarkup};

        assert_eq!(
            SendMessage::builder(3, "text")
                .parse_mode(&ParseMode::Markdown)
                .disable_notification()
                .disable_web_page_preview()
                .reply_to_message_id(6)
                .reply_markup(InlineKeyboardMarkup {
                    inline_keyboard: vec![vec![Box::new(InlineKeyboardButton::new_callback(
                        "text",
                        "callback_data",
                    ))]],
                })
                .build(),
            SendMessage {
                chat_id: 3,
                text: "text".to_owned(),
                parse_mode: Some("markdown".to_owned()),
                disable_web_page_preview: Some(true),
                disable_notification: Some(true),
                reply_to_message_id: Some(6),
                reply_markup: Some(InlineKeyboardMarkup {
                    inline_keyboard: vec![vec![Box::new(InlineKeyboardButton::new_callback(
                        "text",
                        "callback_data",
                    ))]],
                }),
            }
        );
    }
}

// ------------------------------------------------------------------------

/// `/getUpdates`
///
/// [API Doc](https://core.telegram.org/bots/api#getupdates)
///
/// The response is of type [`UpdatesResponse`](../api/type.UpdatesResponse.html)
#[derive(Serialize, Debug)]
pub struct GetUpdates {
    #[serde(skip_serializing_if = "Option::is_none")]
    offset: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    limit: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    timeout: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    allowed_updates: Option<Vec<String>>,
}

impl GetUpdates {
    /// Default timeout is 30 sec
    pub fn new(offset: i32) -> Self {
        GetUpdates {
            offset: Some(offset),
            limit: None,
            timeout: Some(30),
            allowed_updates: None,
        }
    }
}

// ----------------------------------------------------------------------------

mod edit_message {
    use serde::Serialize;

    use super::ParseMode;
    use crate::api::ReplayMarkup;

    /// `/editMessageText`
    ///
    /// [API Doc](https://core.telegram.org/bots/api#editmessagetext)
    ///
    /// The response is of type [`MessageResponse`](../api/type.MessageResponse.html)
    #[derive(Serialize, Debug)]
    pub struct EditMessageText<T: ReplayMarkup> {
        text: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        chat_id: Option<i32>,
        #[serde(skip_serializing_if = "Option::is_none")]
        message_id: Option<i32>,
        #[serde(skip_serializing_if = "Option::is_none")]
        inline_message_id: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        disable_web_page_preview: Option<bool>,
        #[serde(skip_serializing_if = "Option::is_none")]
        reply_markup: Option<T>,
    }

    pub struct EditMessageTextBuilder<T: ReplayMarkup> {
        inner: EditMessageText<T>,
    }

    impl<T> EditMessageTextBuilder<T>
    where
        T: ReplayMarkup,
    {
        pub fn parse_mode(mut self, mode: &ParseMode) -> Self {
            self.inner.parse_mode = match *mode {
                ParseMode::None => None,
                ParseMode::Markdown => Some("markdown".to_owned()),
                ParseMode::HTML => Some("html".to_owned()),
            };

            self
        }

        pub fn inline_message_id(mut self, inline_message_id: Option<String>) -> Self {
            self.inner.inline_message_id = inline_message_id;

            self
        }

        pub fn disable_web_page_preview(mut self) -> Self {
            self.inner.disable_web_page_preview = Some(true);

            self
        }

        pub fn reply_markup(mut self, markup: T) -> Self {
            self.inner.reply_markup = Some(markup);

            self
        }

        pub fn build(self) -> EditMessageText<T> {
            self.inner
        }
    }

    impl<T> EditMessageText<T>
    where
        T: ReplayMarkup,
    {
        pub fn builder(chat_id: i32, message_id: i32, text: &str) -> EditMessageTextBuilder<T> {
            EditMessageTextBuilder {
                inner: EditMessageText {
                    chat_id: Some(chat_id),
                    message_id: Some(message_id),
                    text: text.to_owned(),
                    parse_mode: None,
                    inline_message_id: None,
                    disable_web_page_preview: None,
                    reply_markup: None,
                },
            }
        }
    }

    impl EditMessageText<()> {
        pub fn new(chat_id: i32, message_id: i32, text: &str) -> Self {
            EditMessageText {
                chat_id: Some(chat_id),
                message_id: Some(message_id),
                text: text.to_owned(),
                parse_mode: None,
                inline_message_id: None,
                disable_web_page_preview: None,
                reply_markup: None,
            }
        }

        pub fn markdown(chat_id: i32, message_id: i32, text: &str) -> Self {
            EditMessageText {
                chat_id: Some(chat_id),
                message_id: Some(message_id),
                text: text.to_owned(),
                parse_mode: Some("markdown".to_owned()),
                inline_message_id: None,
                disable_web_page_preview: None,
                reply_markup: None,
            }
        }

        pub fn html(chat_id: i32, message_id: i32, text: &str) -> Self {
            EditMessageText {
                chat_id: Some(chat_id),
                message_id: Some(message_id),
                text: text.to_owned(),
                parse_mode: Some("html".to_owned()),
                inline_message_id: None,
                disable_web_page_preview: None,
                reply_markup: None,
            }
        }
    }
}

// ----------------------------------------------------------------------------

/// `/deleteMessage`
///
/// [API Doc](https://core.telegram.org/bots/api#deletemessage)
///
/// The response is of type [`BoolResponse`](../api/type.BoolResponse.html)
#[derive(Serialize, Debug)]
pub struct DeleteMessage {
    chat_id: i32,
    message_id: i32,
}

impl DeleteMessage {
    pub fn new(chat_id: i32, message_id: i32) -> Self {
        DeleteMessage {
            chat_id,
            message_id,
        }
    }
}
