#![allow(clippy::too_many_arguments, clippy::vec_box)]

use thiserror::Error;

use crate::api::ApiResponse;

pub mod api;
pub mod methods;

#[derive(Debug, Error)]
pub enum TelegramApiError {
    #[error("Missing `{field}` in {clazz}")]
    MissingField { field: String, clazz: String },
    #[error("{description} ({error_code:?})")]
    ErrorResponse {
        description: String,
        error_code: Option<i32>,
    },
}

impl TelegramApiError {
    pub fn field(field: &'static str, clazz: &'static str) -> Self {
        TelegramApiError::MissingField {
            field: field.to_owned(),
            clazz: clazz.to_owned(),
        }
    }
}

impl<T> From<ApiResponse<T>> for TelegramApiError {
    fn from(error: ApiResponse<T>) -> Self {
        TelegramApiError::ErrorResponse {
            description: error.description.unwrap(),
            error_code: error.error_code,
        }
    }
}
