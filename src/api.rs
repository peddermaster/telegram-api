use std::fmt::Debug;

use serde::{Deserialize, Serialize};

pub use self::{
    animation::*, audio::*, callback_query::*, chat::*, chosen_inline_result::*, contact::*,
    document::*, file::*, inline_keyboard_button::*, inline_keyboard_markup::*, inline_query::*,
    inline_query_result::*, input_message_content::*, keyboard_button::*, location::*, message::*,
    message_entity::*, photo_size::*, replay_keyboard_markup::*, replay_keyboard_remove::*,
    update::*, user::*, user_profile_photos::*, venue::*, video::*, video_note::*, voice::*,
};
use super::TelegramApiError;

mod update {
    use serde::{Deserialize, Serialize};

    use super::{
        CallbackQuery, ChosenInlineResult, InlineQuery, Message, PreCheckoutQuery, ShippingQuery,
        TelegramApiError,
    };

    /// This object represents an incoming update.
    ///
    /// At most **one** of the optional parameters can be present in any given update.
    ///
    /// [Telegram Bot API - Update](https://core.telegram.org/bots/api#update)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Update {
        update_id: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        message: Option<Box<Message>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        edited_message: Option<Box<Message>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        channel_post: Option<Box<Message>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        edited_channel_post: Option<Box<Message>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        inline_query: Option<Box<InlineQuery>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        chosen_inline_result: Option<Box<ChosenInlineResult>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        callback_query: Option<Box<CallbackQuery>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        shipping_query: Option<Box<ShippingQuery>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pre_checkout_query: Option<Box<PreCheckoutQuery>>,
    }

    impl<'a> Update {
        /// The update‘s unique identifier. Update identifiers start from a certain positive number
        /// and increase sequentially. This ID becomes especially handy if you’re using
        /// [Webhooks](https://core.telegram.org/bots/api#setwebhook), since it allows you to
        /// ignore repeated updates or to restore the correct update sequence, should they get out
        /// of order. If there are no new updates for at least a week, then identifier of the next
        /// update will be chosen randomly instead of sequentially.
        pub fn id(&self) -> i32 {
            self.update_id
        }

        /// New incoming message of any kind — text, photo, sticker, etc.
        pub fn message(&'a self) -> Result<&'a Message, TelegramApiError> {
            self.message
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("message", "Update"))
        }

        /// New version of a message that is known to the bot and was edited
        pub fn edited_message(&'a self) -> Result<&'a Message, TelegramApiError> {
            self.edited_message
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("edited_message", "Update"))
        }

        /// New incoming channel post of any kind — text, photo, sticker, etc.
        pub fn channel_post(&'a self) -> Result<&'a Message, TelegramApiError> {
            self.channel_post
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("channel_post", "Update"))
        }

        /// New version of a channel post that is known to the bot and was edited
        pub fn edited_channel_post(&'a self) -> Result<&'a Message, TelegramApiError> {
            self.edited_channel_post
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("edited_channel_post", "Update"))
        }

        /// New incoming [inline](https://core.telegram.org/bots/api#inline-mode) query
        pub fn inline_query(&'a self) -> Result<&'a InlineQuery, TelegramApiError> {
            self.inline_query
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("inline_query", "Update"))
        }

        /// The result of an [inline](https://core.telegram.org/bots/api#inline-mode) query that
        /// was chosen by a user and sent to their chat partner. Please see our documentation on
        /// the [feedback collecting](https://core.telegram.org/bots/inline#collecting-feedback)
        /// for details on how to enable these updates for your bot.
        pub fn chosen_inline_result(&'a self) -> Result<&'a ChosenInlineResult, TelegramApiError> {
            self.chosen_inline_result
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("chosen_inline_result", "Update"))
        }

        /// New incoming callback query
        pub fn callback_query(&'a self) -> Result<&'a CallbackQuery, TelegramApiError> {
            self.callback_query
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("callback_query", "Update"))
        }

        /// New incoming shipping query. Only for invoices with flexible price
        pub fn shipping_query(&'a self) -> Result<&'a ShippingQuery, TelegramApiError> {
            self.shipping_query
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("shipping_query", "Update"))
        }

        /// New incoming pre-checkout query. Contains full information about checkout
        pub fn pre_checkout_query(&'a self) -> Result<&'a PreCheckoutQuery, TelegramApiError> {
            self.pre_checkout_query
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("pre_checkout_query", "Update"))
        }
    }
}

mod user {
    use serde::{Deserialize, Serialize};

    use super::TelegramApiError;

    /// This object represents a Telegram user or bot.
    ///
    /// [Telegram Bot API - User](https://core.telegram.org/bots/api#user)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct User {
        id: i32,
        is_bot: bool,
        first_name: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        last_name: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        username: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        language_code: Option<String>,
    }

    /// Field getters
    impl User {
        /// Unique identifier for this user or bot
        pub fn id(&self) -> i32 {
            self.id
        }

        /// True, if this user is a bot
        pub fn is_bot(&self) -> bool {
            self.is_bot
        }

        /// User‘s or bot’s first name
        pub fn first_name(&self) -> String {
            self.first_name.clone()
        }

        /// User‘s or bot’s last name
        pub fn last_name(&self) -> Result<String, TelegramApiError> {
            self.last_name
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("last_name", "User"))
        }

        /// User‘s or bot’s username
        pub fn username(&self) -> Result<String, TelegramApiError> {
            self.username
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("username", "User"))
        }

        /// [IETF language tag](https://en.wikipedia.org/wiki/IETF_language_tag) of the user's language
        pub fn language_code(&self) -> Result<String, TelegramApiError> {
            self.language_code
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("language_code", "User"))
        }
    }
}

mod chat {
    use serde::{Deserialize, Serialize};

    use super::{ChatPhoto, Message, TelegramApiError};

    /// This object represents a chat.
    ///
    /// [Telegram Bot API - Chat](https://core.telegram.org/bots/api#chat)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Chat {
        id: i32,
        #[serde(rename = "type")]
        typ: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        title: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        username: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        first_name: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        last_name: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        all_members_are_administrators: Option<bool>,
        #[serde(skip_serializing_if = "Option::is_none")]
        photo: Option<Box<ChatPhoto>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        description: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        invite_link: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pinned_message: Option<Box<Message>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        sticker_set_name: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        can_set_sticker_set: Option<bool>,
    }

    /// Field getters
    impl<'a> Chat {
        /// Unique identifier for this chat. This number may be greater than 32 bits and some
        /// programming languages may have difficulty/silent defects in interpreting it. But it is
        /// smaller than 52 bits, so a signed 64 bit integer or double-precision float type are
        /// safe for storing this identifier.
        pub fn id(&self) -> i32 {
            self.id
        }

        /// Type of chat, can be either “private”, “group”, “supergroup” or “channel”
        pub fn typ(&self) -> String {
            self.typ.clone()
        }

        /// Title, for supergroups, channels and group chats
        pub fn title(&self) -> Result<String, TelegramApiError> {
            self.title
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("title", "Chat"))
        }

        /// Username, for private chats, supergroups and channels if available
        pub fn username(&self) -> Result<String, TelegramApiError> {
            self.username
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("username", "Chat"))
        }

        /// First name of the other party in a private chat
        pub fn first_name(&self) -> Result<String, TelegramApiError> {
            self.first_name
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("first_name", "Chat"))
        }

        /// Last name of the other party in a private chat
        pub fn last_name(&self) -> Result<String, TelegramApiError> {
            self.last_name
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("last_name", "Chat"))
        }

        /// True if a group has ‘All Members Are Admins’ enabled.
        pub fn all_members_are_administrators(&self) -> Result<bool, TelegramApiError> {
            self.all_members_are_administrators
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("all_members_are_administrators", "Chat"))
        }

        /// Chat photo. Returned only in [`getChat`](https://core.telegram.org/bots/api#getchat).
        pub fn photo(&'a self) -> Result<&'a ChatPhoto, TelegramApiError> {
            self.photo
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("photo", "Chat"))
        }

        /// Description, for supergroups and channel chats. Returned only in
        /// [`getChat`](https://core.telegram.org/bots/api#getchat).
        pub fn description(&self) -> Result<String, TelegramApiError> {
            self.description
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("description", "Chat"))
        }

        /// Chat invite link, for supergroups and channel chats. Returned only in
        /// [`getChat`](https://core.telegram.org/bots/api#getchat).
        pub fn invite_link(&self) -> Result<String, TelegramApiError> {
            self.invite_link
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("invite_link", "Chat"))
        }

        /// Pinned message, for supergroups and channel chats. Returned only in
        /// [`getChat`](https://core.telegram.org/bots/api#getchat).
        pub fn pinned_message(&'a self) -> Result<&'a Message, TelegramApiError> {
            self.pinned_message
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("pinned_message", "Chat"))
        }

        /// For supergroups, name of group sticker set. Returned only in
        /// [`getChat`](https://core.telegram.org/bots/api#getchat).
        pub fn sticker_set_name(&self) -> Result<String, TelegramApiError> {
            self.sticker_set_name
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("sticker_set_name", "Chat"))
        }

        /// True, if the bot can change the group sticker set. Returned only in
        /// [`getChat`](https://core.telegram.org/bots/api#getchat).
        pub fn can_set_sticker_set(&self) -> Result<bool, TelegramApiError> {
            self.can_set_sticker_set
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("can_set_sticker_set", "Chat"))
        }
    }
}

mod message {
    use serde::{Deserialize, Serialize};

    use super::{
        Animation, Audio, Chat, Contact, Document, Game, Invoice, Location, MessageEntity,
        PassportData, PhotoSize, Sticker, SuccessfulPayment, TelegramApiError, User, Venue, Video,
        VideoNote, Voice,
    };

    /// This object represents a message.
    ///
    /// [Telegram Bot API - Message](https://core.telegram.org/bots/api#message)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Message {
        message_id: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        from: Option<Box<User>>,
        date: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        chat: Option<Box<Chat>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        forward_from: Option<Box<User>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        forward_from_chat: Option<Box<Chat>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        forward_from_message_id: Option<i32>,
        #[serde(skip_serializing_if = "Option::is_none")]
        forward_signature: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        forward_date: Option<i32>,
        #[serde(skip_serializing_if = "Option::is_none")]
        reply_to_message: Option<Box<Message>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        edit_date: Option<i32>,
        #[serde(skip_serializing_if = "Option::is_none")]
        media_group_id: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        author_signature: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        text: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        entities: Option<Vec<Box<MessageEntity>>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        caption_entities: Option<Vec<Box<MessageEntity>>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        audio: Option<Box<Audio>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        document: Option<Box<Document>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        animation: Option<Box<Animation>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        game: Option<Box<Game>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        photo: Option<Vec<Box<PhotoSize>>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        sticker: Option<Box<Sticker>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        video: Option<Box<Video>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        voice: Option<Box<Voice>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        video_note: Option<Box<VideoNote>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        contact: Option<Box<Contact>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        location: Option<Box<Location>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        venue: Option<Box<Venue>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        new_chat_members: Option<Vec<Box<User>>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        left_chat_member: Option<Box<User>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        new_chat_title: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        new_chat_photo: Option<Vec<Box<PhotoSize>>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        delete_chat_photo: Option<bool>,
        #[serde(skip_serializing_if = "Option::is_none")]
        group_chat_created: Option<bool>,
        #[serde(skip_serializing_if = "Option::is_none")]
        supergroup_chat_created: Option<bool>,
        #[serde(skip_serializing_if = "Option::is_none")]
        channel_chat_created: Option<bool>,
        #[serde(skip_serializing_if = "Option::is_none")]
        migrate_to_chat_id: Option<i32>,
        #[serde(skip_serializing_if = "Option::is_none")]
        migrate_from_chat_id: Option<i32>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pinned_message: Option<Box<Message>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        invoice: Option<Box<Invoice>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        successful_payment: Option<Box<SuccessfulPayment>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        connected_website: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        passport_data: Option<Box<PassportData>>,
    }

    /// Field getters
    impl<'a> Message {
        /// Unique message identifier inside this chat
        pub fn id(&self) -> i32 {
            self.message_id
        }

        /// Sender, empty for messages sent to channels
        pub fn from(&'a self) -> Result<&'a User, TelegramApiError> {
            self.from
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("from", "Message"))
        }

        /// Date the message was sent in Unix time
        pub fn date(&self) -> i32 {
            self.date
        }

        /// Conversation the message belongs to
        pub fn chat(&'a self) -> Result<&'a Chat, TelegramApiError> {
            self.chat
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("chat", "Message"))
        }

        /// For forwarded messages, sender of the original message
        pub fn forward_from(&'a self) -> Result<&'a User, TelegramApiError> {
            self.forward_from
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("forward_from", "Message"))
        }

        /// For messages forwarded from channels, information about the original channel
        pub fn forward_from_chat(&'a self) -> Result<&'a Chat, TelegramApiError> {
            self.forward_from_chat
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("forward_from_chat", "Message"))
        }

        /// For messages forwarded from channels, identifier of the original message in the channel
        pub fn forward_from_message_id(&self) -> Result<i32, TelegramApiError> {
            self.forward_from_message_id
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("forward_from_message_id", "Message"))
        }

        /// For messages forwarded from channels, signature of the post author if present
        pub fn forward_signature(&self) -> Result<String, TelegramApiError> {
            self.forward_signature
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("forward_signature", "Message"))
        }

        /// For forwarded messages, date the original message was sent in Unix time
        pub fn forward_date(&self) -> Result<i32, TelegramApiError> {
            self.forward_date
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("forward_date", "Message"))
        }

        /// For replies, the original message. Note that the Message object in this field will not
        /// contain further reply_to_message fields even if it itself is a reply.
        pub fn reply_to_message(&'a self) -> Result<&'a Message, TelegramApiError> {
            self.reply_to_message
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("reply_to_message", "Message"))
        }

        /// Date the message was last edited in Unix time
        pub fn edit_date(&self) -> Result<i32, TelegramApiError> {
            self.edit_date
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("edit_date", "Message"))
        }

        /// The unique identifier of a media message group this message belongs to
        pub fn media_group_id(&self) -> Result<String, TelegramApiError> {
            self.media_group_id
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("media_group_id", "Message"))
        }

        /// Signature of the post author for messages in channels
        pub fn author_signature(&self) -> Result<String, TelegramApiError> {
            self.author_signature
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("author_signature", "Message"))
        }

        /// For text messages, the actual UTF-8 text of the message, 0-4096 characters.
        pub fn text(&self) -> Result<String, TelegramApiError> {
            self.text
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("text", "Message"))
        }

        /// For text messages, special entities like usernames, URLs, bot commands, etc. that
        /// appear in the text
        pub fn entities(&'a self) -> Result<Vec<&'a MessageEntity>, TelegramApiError> {
            Ok(self
                .entities
                .as_ref()
                .ok_or_else(|| TelegramApiError::field("entities", "Message"))?
                .iter()
                .map(|m| m.as_ref())
                .collect())
        }

        /// For messages with a caption, special entities like usernames, URLs, bot commands, etc. that appear in the caption
        pub fn caption_entities(&'a self) -> Result<Vec<&'a MessageEntity>, TelegramApiError> {
            Ok(self
                .caption_entities
                .as_ref()
                .ok_or_else(|| TelegramApiError::field("caption_entities", "Message"))?
                .iter()
                .map(|m| m.as_ref())
                .collect())
        }

        /// Message is an audio file, information about the file
        pub fn audio(&'a self) -> Result<&'a Audio, TelegramApiError> {
            self.audio
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("audio", "Message"))
        }

        /// Message is a general file, information about the file
        pub fn document(&'a self) -> Result<&'a Document, TelegramApiError> {
            self.document
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("document", "Message"))
        }

        /// Message is an animation, information about the animation. For backward compatibility,
        /// when this field is set, the document field will also be set
        pub fn animation(&'a self) -> Result<&'a Animation, TelegramApiError> {
            self.animation
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("animation", "Message"))
        }

        /// Message is a game, information about the game.
        /// [More about games »](https://core.telegram.org/bots/api#games)
        pub fn game(&'a self) -> Result<&'a Game, TelegramApiError> {
            self.game
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("game", "Message"))
        }

        /// Message is a photo, available sizes of the photo
        pub fn photo(&'a self) -> Result<Vec<&'a PhotoSize>, TelegramApiError> {
            Ok(self
                .photo
                .as_ref()
                .ok_or_else(|| TelegramApiError::field("photo", "Message"))?
                .iter()
                .map(|m| m.as_ref())
                .collect())
        }

        /// Message is a sticker, information about the sticker
        pub fn sticker(&'a self) -> Result<&'a Sticker, TelegramApiError> {
            self.sticker
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("sticker", "Message"))
        }

        /// Message is a video, information about the video
        pub fn video(&'a self) -> Result<&'a Video, TelegramApiError> {
            self.video
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("video", "Message"))
        }

        /// Message is a voice message, information about the file
        pub fn voice(&'a self) -> Result<&'a Voice, TelegramApiError> {
            self.voice
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("voice", "Message"))
        }

        /// Message is a [video note](https://telegram.org/blog/video-messages-and-telescope),
        /// information about the video message
        pub fn video_note(&'a self) -> Result<&'a VideoNote, TelegramApiError> {
            self.video_note
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("video_note", "Message"))
        }

        /// Caption for the audio, document, photo, video or voice, 0-200 characters
        pub fn caption(&self) -> Result<String, TelegramApiError> {
            self.caption
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("caption", "Message"))
        }

        /// Message is a shared contact, information about the contact
        pub fn contact(&'a self) -> Result<&'a Contact, TelegramApiError> {
            self.contact
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("contact", "Message"))
        }

        /// Message is a shared location, information about the location
        pub fn location(&'a self) -> Result<&'a Location, TelegramApiError> {
            self.location
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("location", "Message"))
        }

        /// Message is a venue, information about the venue
        pub fn venue(&'a self) -> Result<&'a Venue, TelegramApiError> {
            self.venue
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("venue", "Message"))
        }

        /// New members that were added to the group or supergroup and information about them
        /// (the bot itself may be one of these members)
        pub fn new_chat_members(&'a self) -> Result<Vec<&'a User>, TelegramApiError> {
            Ok(self
                .new_chat_members
                .as_ref()
                .ok_or_else(|| TelegramApiError::field("new_chat_members", "Message"))?
                .iter()
                .map(|m| m.as_ref())
                .collect())
        }

        /// A member was removed from the group, information about them (this member may be the bot
        /// itself)
        pub fn left_chat_member(&'a self) -> Result<&'a User, TelegramApiError> {
            self.left_chat_member
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("left_chat_member", "Message"))
        }

        /// A chat title was changed to this value
        pub fn new_chat_title(&self) -> Result<String, TelegramApiError> {
            self.new_chat_title
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("new_chat_title", "Message"))
        }

        /// A chat photo was change to this value
        pub fn new_chat_photo(&'a self) -> Result<Vec<&'a PhotoSize>, TelegramApiError> {
            Ok(self
                .new_chat_photo
                .as_ref()
                .ok_or_else(|| TelegramApiError::field("new_chat_photo", "Message"))?
                .iter()
                .map(|m| m.as_ref())
                .collect())
        }

        /// Service message: the chat photo was deleted
        pub fn delete_chat_photo(&self) -> Result<bool, TelegramApiError> {
            self.delete_chat_photo
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("delete_chat_photo", "Message"))
        }

        /// Service message: the group has been created
        pub fn group_chat_created(&self) -> Result<bool, TelegramApiError> {
            self.group_chat_created
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("group_chat_created", "Message"))
        }

        /// Service message: the supergroup has been created. This field can‘t be received in a
        /// message coming through updates, because bot can’t be a member of a supergroup when it
        /// is created. It can only be found in reply_to_message if someone replies to a very first
        /// message in a directly created supergroup.
        pub fn supergroup_chat_created(&self) -> Result<bool, TelegramApiError> {
            self.supergroup_chat_created
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("supergroup_chat_created", "Message"))
        }

        /// Service message: the channel has been created. This field can‘t be received in a
        /// message coming through updates, because bot can’t be a member of a channel when it is
        /// created. It can only be found in reply_to_message if someone replies to a very first
        /// message in a channel.
        pub fn channel_chat_created(&self) -> Result<bool, TelegramApiError> {
            self.channel_chat_created
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("channel_chat_created", "Message"))
        }

        /// The group has been migrated to a supergroup with the specified identifier. This number
        /// may be greater than 32 bits and some programming languages may have difficulty/silent
        /// defects in interpreting it. But it is smaller than 52 bits, so a signed 64 bit integer
        /// or double-precision float type are safe for storing this identifier.
        pub fn migrate_to_chat_id(&self) -> Result<i32, TelegramApiError> {
            self.migrate_to_chat_id
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("migrate_to_chat_id", "Message"))
        }

        /// The supergroup has been migrated from a group with the specified identifier. This
        /// number may be greater than 32 bits and some programming languages may have
        /// difficulty/silent defects in interpreting it. But it is smaller than 52 bits, so a
        /// signed 64 bit integer or double-precision float type are safe for storing this
        /// identifier.
        pub fn migrate_from_chat_id(&self) -> Result<i32, TelegramApiError> {
            self.migrate_from_chat_id
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("migrate_from_chat_id", "Message"))
        }

        /// Specified message was pinned. Note that the Message object in this field will not
        /// contain further reply_to_message fields even if it is itself a reply.
        pub fn pinned_message(&'a self) -> Result<&'a Message, TelegramApiError> {
            self.pinned_message
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("pinned_message", "Message"))
        }

        /// Message is an invoice for a [payment](https://core.telegram.org/bots/api#payments),
        /// information about the invoice.
        /// [More about payments »](https://core.telegram.org/bots/api#payments)
        pub fn invoice(&'a self) -> Result<&'a Invoice, TelegramApiError> {
            self.invoice
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("invoice", "Message"))
        }

        /// Optional. Message is a service message about a successful payment, information about
        /// the payment. [More about payments »](https://core.telegram.org/bots/api#payments)
        pub fn successful_payment(&'a self) -> Result<&'a SuccessfulPayment, TelegramApiError> {
            self.successful_payment
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("successful_payment", "Message"))
        }

        /// The domain name of the website on which the user has logged in.
        /// [More about Telegram Login »](https://core.telegram.org/widgets/login)
        pub fn connected_website(&self) -> Result<String, TelegramApiError> {
            self.connected_website
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("connected_website", "Message"))
        }

        /// Telegram Passport data
        pub fn passport_data(&'a self) -> Result<&'a PassportData, TelegramApiError> {
            self.passport_data
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("passport_data", "Message"))
        }
    }

    /// Extra functions
    impl Message {
        pub fn contains_bot_command(&self) -> bool {
            if let Ok(entites) = self.entities() {
                entites.iter().any(|e| e.is_bot_command())
            } else {
                false
            }
        }
    }
}

mod message_entity {
    use serde::{Deserialize, Serialize};

    use super::{TelegramApiError, User};

    /// This object represents one special entity in a text message. For example, hashtags,
    /// usernames, URLs, etc.
    ///
    /// [Telegram Bot API - MessageEntity](https://core.telegram.org/bots/api#messageentity)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct MessageEntity {
        #[serde(rename = "type")]
        typ: String,
        offset: i32,
        length: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        url: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        user: Option<Box<User>>,
    }

    /// Field getters
    impl<'a> MessageEntity {
        /// Type of the entity. Can be *mention* (`@username`), *hashtag*, *cashtag*,
        /// *bot_command*, *url*, *email*, *phone_number*, *bold* (bold text), *italic* (italic
        /// text), *code* (monowidth string), *pre* (monowidth block), *text_link* (for clickable
        /// text URLs), *text_mention* (for users
        /// [without usernames](https://telegram.org/blog/edit#new-mentions))
        pub fn typ(&self) -> String {
            self.typ.clone()
        }

        /// Offset in UTF-16 code units to the start of the entity
        pub fn offset(&self) -> i32 {
            self.offset
        }

        /// Length of the entity in UTF-16 code units
        pub fn length(&self) -> i32 {
            self.length
        }

        /// For “text_link” only, url that will be opened after user taps on the text
        pub fn url(&self) -> Result<String, TelegramApiError> {
            self.url
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("url", "MessageEntity"))
        }

        /// For “text_mention” only, the mentioned user
        pub fn user(&'a self) -> Result<&'a User, TelegramApiError> {
            self.user
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("user", "MessageEntity"))
        }
    }

    /// Extra functions
    impl MessageEntity {
        pub fn is_bot_command(&self) -> bool {
            self.typ == "bot_command"
        }
    }
}

mod photo_size {
    use serde::{Deserialize, Serialize};

    use super::TelegramApiError;

    /// This object represents one size of a photo or a
    /// [file](https://core.telegram.org/bots/api#document) /
    /// [sticker](https://core.telegram.org/bots/api#sticker) thumbnail.
    ///
    /// [Telegram Bot API - PhotoSize](https://core.telegram.org/bots/api#photosize)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct PhotoSize {
        file_id: String,
        width: i32,
        height: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        file_size: Option<i32>,
    }

    /// Field getters
    impl PhotoSize {
        /// Unique identifier for this file
        pub fn file_id(&self) -> String {
            self.file_id.clone()
        }

        /// Photo width
        pub fn width(&self) -> i32 {
            self.width
        }

        /// Photo height
        pub fn height(&self) -> i32 {
            self.height
        }

        /// File size
        pub fn file_size(&self) -> Result<i32, TelegramApiError> {
            self.file_size
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_size", "PhotoSize"))
        }
    }
}

mod audio {
    use serde::{Deserialize, Serialize};

    use super::{PhotoSize, TelegramApiError};

    /// This object represents an audio file to be treated as music by the Telegram clients.
    ///
    /// [Telegram Bot API - Audio](https://core.telegram.org/bots/api#audio)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Audio {
        file_id: String,
        duration: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        performer: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        title: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        mime_type: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        file_size: Option<i32>,
        #[serde(skip_serializing_if = "Option::is_none")]
        thumb: Option<Box<PhotoSize>>,
    }

    /// Field getters
    impl<'a> Audio {
        /// Unique identifier for this file
        pub fn file_id(&self) -> String {
            self.file_id.clone()
        }

        /// Duration of the audio in seconds as defined by sender
        pub fn duration(&self) -> i32 {
            self.duration
        }

        /// Performer of the audio as defined by sender or by audio tags
        pub fn performer(&self) -> Result<String, TelegramApiError> {
            self.performer
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("performer", "Audio"))
        }

        /// Title of the audio as defined by sender or by audio tags
        pub fn title(&self) -> Result<String, TelegramApiError> {
            self.title
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("title", "Audio"))
        }

        /// MIME type of the file as defined by sender
        pub fn mime_type(&self) -> Result<String, TelegramApiError> {
            self.mime_type
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("mime_type", "Audio"))
        }

        /// File size
        pub fn file_size(&self) -> Result<i32, TelegramApiError> {
            self.file_size
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_size", "Audio"))
        }

        /// Thumbnail of the album cover to which the music file belongs
        pub fn thumb(&'a self) -> Result<&'a PhotoSize, TelegramApiError> {
            self.thumb
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("thumb", "Audio"))
        }
    }
}

mod document {
    use serde::{Deserialize, Serialize};

    use super::{PhotoSize, TelegramApiError};

    /// This object represents a general file (as opposed to
    /// [photos](https://core.telegram.org/bots/api#photosize),
    /// [voice messages](https://core.telegram.org/bots/api#voice) and
    /// [audio files](https://core.telegram.org/bots/api#audio)).
    ///
    /// [Telegram Bot API - Document](https://core.telegram.org/bots/api#document)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Document {
        file_id: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        thumb: Option<Box<PhotoSize>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        file_name: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        mime_type: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        file_size: Option<i32>,
    }

    /// Field getters
    impl<'a> Document {
        /// Unique file identifier
        pub fn file_id(&self) -> String {
            self.file_id.clone()
        }

        /// Document thumbnail as defined by sender
        pub fn thumb(&'a self) -> Result<&'a PhotoSize, TelegramApiError> {
            self.thumb
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("thumb", "Document"))
        }

        /// Original filename as defined by sender
        pub fn file_name(&self) -> Result<String, TelegramApiError> {
            self.file_name
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_name", "Document"))
        }

        /// MIME type of the file as defined by sender
        pub fn mime_type(&self) -> Result<String, TelegramApiError> {
            self.mime_type
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("mime_type", "Document"))
        }

        /// File size
        pub fn file_size(&self) -> Result<i32, TelegramApiError> {
            self.file_size
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_size", "Document"))
        }
    }
}

mod video {
    use serde::{Deserialize, Serialize};

    use super::{PhotoSize, TelegramApiError};

    /// This object represents a video file.
    ///
    /// [Telegram Bot API - Video](https://core.telegram.org/bots/api#video)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Video {
        file_id: String,
        width: i32,
        height: i32,
        duration: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        thumb: Option<Box<PhotoSize>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        mime_type: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        file_size: Option<i32>,
    }

    /// Field getters
    impl<'a> Video {
        /// Unique identifier for this file
        pub fn file_id(&self) -> String {
            self.file_id.clone()
        }

        /// Video width as defined by sender
        pub fn width(&self) -> i32 {
            self.width
        }

        /// Video height as defined by sender
        pub fn height(&self) -> i32 {
            self.height
        }

        /// Duration of the video in seconds as defined by sender
        pub fn duration(&self) -> i32 {
            self.duration
        }

        /// Video thumbnail
        pub fn thumb(&'a self) -> Result<&'a PhotoSize, TelegramApiError> {
            self.thumb
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("thumb", "Video"))
        }

        /// Mime type of a file as defined by sender
        pub fn mime_type(&self) -> Result<String, TelegramApiError> {
            self.mime_type
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("mime_type", "Video"))
        }

        /// File size
        pub fn file_size(&self) -> Result<i32, TelegramApiError> {
            self.file_size
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_size", "Video"))
        }
    }
}

mod animation {
    use serde::{Deserialize, Serialize};

    use super::{PhotoSize, TelegramApiError};

    /// This object represents an animation file (GIF or H.264/MPEG-4 AVC video without sound).
    ///
    /// [Telegram Bot API - Animation](https://core.telegram.org/bots/api#animation)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Animation {
        file_id: String,
        width: i32,
        height: i32,
        duration: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        thumb: Option<Box<PhotoSize>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        file_name: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        mime_type: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        file_size: Option<i32>,
    }

    impl<'a> Animation {
        /// Unique file identifier
        pub fn file_id(&self) -> String {
            self.file_id.clone()
        }

        /// Video width as defined by sender
        pub fn width(&self) -> i32 {
            self.width
        }

        /// Video height as defined by sender
        pub fn height(&self) -> i32 {
            self.height
        }

        /// Duration of the video in seconds as defined by sender
        pub fn duration(&self) -> i32 {
            self.duration
        }

        /// Animation thumbnail as defined by sender
        pub fn thumb(&'a self) -> Result<&'a PhotoSize, TelegramApiError> {
            self.thumb
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("thumb", "Animation"))
        }

        /// Original animation filename as defined by sender
        pub fn file_name(&self) -> Result<String, TelegramApiError> {
            self.file_name
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_name", "Animation"))
        }

        /// MIME type of the file as defined by sender
        pub fn mime_type(&self) -> Result<String, TelegramApiError> {
            self.mime_type
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("mime_type", "Animation"))
        }

        /// File size
        pub fn file_size(&self) -> Result<i32, TelegramApiError> {
            self.file_size
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_size", "Animation"))
        }
    }
}

mod voice {
    use serde::{Deserialize, Serialize};

    use super::TelegramApiError;

    #[derive(Serialize, Deserialize, Debug)]
    pub struct Voice {
        file_id: String,
        duration: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        mime_type: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        file_size: Option<i32>,
    }

    /// Field getters
    impl Voice {
        pub fn file_id(&self) -> String {
            self.file_id.clone()
        }

        pub fn duration(&self) -> i32 {
            self.duration
        }

        pub fn mime_type(&self) -> Result<String, TelegramApiError> {
            self.mime_type
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("mime_type", "Voice"))
        }

        pub fn file_size(&self) -> Result<i32, TelegramApiError> {
            self.file_size
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_size", "Voice"))
        }
    }
}

mod video_note {
    use serde::{Deserialize, Serialize};

    use super::{PhotoSize, TelegramApiError};

    #[derive(Serialize, Deserialize, Debug)]
    pub struct VideoNote {
        file_id: String,
        length: i32,
        duration: i32,
        thumb: Option<Box<PhotoSize>>,
        file_size: Option<i32>,
    }

    /// Field getters
    impl<'a> VideoNote {
        pub fn file_id(&self) -> String {
            self.file_id.clone()
        }

        pub fn length(&self) -> i32 {
            self.length
        }

        pub fn duration(&self) -> i32 {
            self.duration
        }

        pub fn thumb(&'a self) -> Result<&'a PhotoSize, TelegramApiError> {
            self.thumb
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("thumb", "VideoNote"))
        }

        pub fn file_size(&self) -> Result<i32, TelegramApiError> {
            self.file_size
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_size", "VideoNote"))
        }
    }
}

mod contact {
    use serde::{Deserialize, Serialize};

    use super::TelegramApiError;

    #[derive(Serialize, Deserialize, Debug)]
    pub struct Contact {
        phone_number: String,
        first_name: String,
        last_name: Option<String>,
        user_id: Option<i32>,
    }

    /// Field getters
    impl Contact {
        pub fn phone_number(&self) -> String {
            self.phone_number.clone()
        }

        pub fn first_name(&self) -> String {
            self.first_name.clone()
        }

        pub fn last_name(&self) -> Result<String, TelegramApiError> {
            self.last_name
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("last_name", "Contact"))
        }

        pub fn user_id(&self) -> Result<i32, TelegramApiError> {
            self.user_id
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("user_id", "Contact"))
        }
    }
}

mod location {
    use serde::{Deserialize, Serialize};

    #[derive(Serialize, Deserialize, Debug)]
    pub struct Location {
        longitude: f64,
        latitude: f64,
    }

    /// Field getters
    impl Location {
        pub fn longitude(&self) -> f64 {
            self.longitude
        }

        pub fn latitude(&self) -> f64 {
            self.latitude
        }
    }
}

mod venue {
    use serde::{Deserialize, Serialize};

    use super::{Location, TelegramApiError};

    #[derive(Serialize, Deserialize, Debug)]
    pub struct Venue {
        location: Box<Location>,
        title: String,
        address: String,
        foursquare_id: Option<i32>,
    }

    /// Field getters
    impl<'a> Venue {
        pub fn location(&'a self) -> &'a Location {
            &self.location
        }

        pub fn title(&self) -> String {
            self.title.clone()
        }

        pub fn address(&self) -> String {
            self.address.clone()
        }

        pub fn foursquare_id(&self) -> Result<i32, TelegramApiError> {
            self.foursquare_id
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("foursquare_id", "Venue"))
        }
    }
}

mod user_profile_photos {
    use serde::{Deserialize, Serialize};

    use super::PhotoSize;

    #[derive(Serialize, Deserialize, Debug)]
    pub struct UserProfilePhotos {
        total_count: i32,
        photos: Vec<Vec<Box<PhotoSize>>>,
    }

    /// Field getters
    impl<'a> UserProfilePhotos {
        pub fn total_count(&self) -> i32 {
            self.total_count
        }

        pub fn photos(&'a self) -> Vec<Vec<&'a PhotoSize>> {
            self.photos
                .iter()
                .map(|outer| outer.iter().map(|inner| inner.as_ref()).collect())
                .collect()
        }
    }
}

mod file {
    use serde::{Deserialize, Serialize};

    use super::TelegramApiError;

    #[derive(Serialize, Deserialize, Debug)]
    pub struct File {
        file_id: String,
        file_size: Option<i32>,
        file_path: Option<String>,
    }

    /// Field getters
    impl File {
        pub fn file_id(&self) -> String {
            self.file_id.clone()
        }

        pub fn file_size(&self) -> Result<i32, TelegramApiError> {
            self.file_size
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_size", "File"))
        }

        pub fn file_path(&self) -> Result<String, TelegramApiError> {
            self.file_path
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("file_path", "File"))
        }
    }
}

mod replay_keyboard_markup {
    use serde::{Deserialize, Serialize};

    use super::{KeyboardButton, TelegramApiError};

    #[derive(Serialize, Deserialize, Debug)]
    pub struct ReplyKeyboardMarkup {
        keyboard: Vec<Vec<Box<KeyboardButton>>>,
        resize_keyboard: Option<bool>,
        one_time_keyboard: Option<bool>,
        selective: Option<bool>,
    }

    /// Field getters
    impl<'a> ReplyKeyboardMarkup {
        pub fn keyboard(&'a self) -> Vec<Vec<&'a KeyboardButton>> {
            self.keyboard
                .iter()
                .map(|outer| outer.iter().map(|inner| inner.as_ref()).collect())
                .collect()
        }

        pub fn resize_keyboard(&self) -> Result<bool, TelegramApiError> {
            self.resize_keyboard
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("resize_keyboard", "ReplayKeyboardMarkup"))
        }

        pub fn one_time_keyboard(&self) -> Result<bool, TelegramApiError> {
            self.one_time_keyboard
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("one_time_keyboard", "ReplayKeyboardMarkup"))
        }

        pub fn selective(&self) -> Result<bool, TelegramApiError> {
            self.selective
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("selective", "ReplayKeyboardMarkup"))
        }
    }
}

mod keyboard_button {
    use serde::{Deserialize, Serialize};

    use super::TelegramApiError;

    #[derive(Serialize, Deserialize, Debug)]
    pub struct KeyboardButton {
        text: String,
        request_contact: Option<bool>,
        request_location: Option<bool>,
    }

    /// Field getters
    impl KeyboardButton {
        pub fn text(&self) -> String {
            self.text.clone()
        }

        pub fn request_contact(&self) -> Result<bool, TelegramApiError> {
            self.request_contact
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("request_contact", "KeyboardButton"))
        }

        pub fn request_location(&self) -> Result<bool, TelegramApiError> {
            self.request_location
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("request_location", "KeyboardButton"))
        }
    }
}

mod replay_keyboard_remove {
    use serde::{Deserialize, Serialize};

    use super::TelegramApiError;

    #[derive(Serialize, Deserialize, Debug)]
    pub struct ReplyKeyboardRemove {
        /// Must be set to True
        remove_keyboard: bool,
        selective: Option<bool>,
    }

    /// Field getters
    impl ReplyKeyboardRemove {
        pub fn remove_keyboard(&self) -> bool {
            self.remove_keyboard
        }

        pub fn selective(&self) -> Result<bool, TelegramApiError> {
            self.selective
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("selective", "ReplyKeyboardRemove"))
        }
    }
}

mod inline_keyboard_markup {
    use serde::{Deserialize, Serialize};

    use super::InlineKeyboardButton;

    /// This object represents an
    /// [inline keyboard](https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating)
    /// that appears right next to the message it belongs to.
    ///
    /// [API Doc](https://core.telegram.org/bots/api#inlinekeyboardmarkup)
    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(PartialEq))]
    pub struct InlineKeyboardMarkup {
        /// Array of button rows, each represented by an Array of
        /// [`InlineKeyboardButton`](struct.InlineKeyboardButton.html) objects
        pub inline_keyboard: Vec<Vec<Box<InlineKeyboardButton>>>,
    }

    /// Field getters
    impl<'a> InlineKeyboardMarkup {
        pub fn inline_keyboard(&'a self) -> Vec<Vec<&'a InlineKeyboardButton>> {
            self.inline_keyboard
                .iter()
                .map(|outer| outer.iter().map(|inner| inner.as_ref()).collect())
                .collect()
        }
    }
}

mod inline_keyboard_button {
    use serde::{Deserialize, Serialize};

    use super::{CallbackGame, TelegramApiError};

    /// This object represents one button of an [inline keyboard](struct.InlineKeyboardMarkup.html).
    ///
    /// [API Doc](https://core.telegram.org/bots/api#inlinekeyboardbutton)
    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(PartialEq))]
    pub struct InlineKeyboardButton {
        /// Label text on the button
        text: String,
        /// *Optional.* HTTP url to be opened when button is pressed
        #[serde(skip_serializing_if = "Option::is_none")]
        url: Option<String>,
        /// *Optional.* Data to be sent in a
        /// [callback query](https://core.telegram.org/bots/api#callbackquery)
        /// to the bot when button is pressed, 1-64 bytes
        #[serde(skip_serializing_if = "Option::is_none")]
        callback_data: Option<String>,
        /// *Optional.* If set, pressing the button will prompt the user to select one of their chats,
        /// open that chat and insert the bot‘s username and the specified inline query in the input
        /// field. Can be empty, in which case just the bot’s username will be inserted.
        ///
        /// **Note**: This offers an easy way for users to start using your bot in
        /// [inline mode](https://core.telegram.org/bots/inline) when they are currently in a private
        /// chat with it. Especially useful when combined with
        /// [switch_pm…](https://core.telegram.org/bots/api#answerinlinequery) actions – in this case
        /// the user will be automatically returned to the chat they switched from, skipping the chat
        /// selection screen.
        #[serde(skip_serializing_if = "Option::is_none")]
        switch_inline_query: Option<String>,
        /// *Optional.* If set, pressing the button will insert the bot‘s username and the specified
        /// inline query in the current chat's input field. Can be empty, in which case only the bot’s
        /// username will be inserted.
        ///
        /// This offers a quick way for the user to open your bot in inline mode in the same chat –
        /// good for selecting something from multiple options.
        #[serde(skip_serializing_if = "Option::is_none")]
        switch_inline_query_current_chat: Option<String>,
        /// *Optional*. Description of the game that will be launched when the user presses the button.
        ///
        /// **NOTE**: This type of button **must** always be the first button in the first row.
        #[serde(skip_serializing_if = "Option::is_none")]
        callback_game: Option<Box<CallbackGame>>,
        /// *Optional*. Specify True, to send a [Pay button](https://core.telegram.org/bots/api#payments).
        ///
        /// **NOTE**: This type of button **must** always be the first button in the first row.
        #[serde(skip_serializing_if = "Option::is_none")]
        pay: Option<bool>,
    }

    /// Field getters
    impl<'a> InlineKeyboardButton {
        pub fn text(&self) -> String {
            self.text.clone()
        }

        pub fn url(&self) -> Result<String, TelegramApiError> {
            self.url
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("url", "InlineKeyboardButton"))
        }

        pub fn callback_data(&self) -> Result<String, TelegramApiError> {
            self.callback_data
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("callback_data", "InlineKeyboardButton"))
        }

        pub fn switch_inline_query(&self) -> Result<String, TelegramApiError> {
            self.switch_inline_query.as_ref().cloned().ok_or_else(|| {
                TelegramApiError::field("switch_inline_query", "InlineKeyboardButton")
            })
        }

        pub fn switch_inline_query_current_chat(&self) -> Result<String, TelegramApiError> {
            self.switch_inline_query_current_chat
                .as_ref()
                .cloned()
                .ok_or_else(|| {
                    TelegramApiError::field(
                        "switch_inline_query_current_chat",
                        "InlineKeyboardButton",
                    )
                })
        }

        pub fn callback_game(&'a self) -> Result<&'a CallbackGame, TelegramApiError> {
            self.callback_game
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("callback_game", "InlineKeyboardButton"))
        }

        pub fn pay(&self) -> Result<bool, TelegramApiError> {
            self.pay
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("pay", "InlineKeyboardButton"))
        }
    }

    /// Constructors
    impl InlineKeyboardButton {
        /// Constructor for an url button
        ///
        /// `text`: Label text of the button
        ///
        /// `url`: HTTP url to be opened when button is pressed
        pub fn new_url(text: &str, url: &str) -> Self {
            InlineKeyboardButton {
                text: text.to_owned(),
                url: Some(url.to_owned()),
                callback_data: None,
                switch_inline_query: None,
                switch_inline_query_current_chat: None,
                callback_game: None,
                pay: None,
            }
        }

        /// Constructor for a callback button
        ///
        /// `text`: Label text of the button
        ///
        /// `callback_data`: Data to be sent in a
        /// [callback query](https://core.telegram.org/bots/api#callbackquery)
        /// to the bot when button is pressed, 1-64 bytes
        pub fn new_callback(text: &str, callback_data: &str) -> Self {
            InlineKeyboardButton {
                text: text.to_owned(),
                url: None,
                callback_data: Some(callback_data.to_owned()),
                switch_inline_query: None,
                switch_inline_query_current_chat: None,
                callback_game: None,
                pay: None,
            }
        }

        /// Constructor for a switch_inline button
        ///
        /// `text`: Label text of the button
        ///
        /// `switch_inline_query`: If set, pressing the button will prompt the user to select one of
        /// their chats, open that chat and insert the bot‘s username and the specified inline query
        /// in the input field. Can be empty, in which case just the bot’s username will be inserted.
        pub fn new_switch_inline(text: &str, switch_inline_query: &str) -> Self {
            InlineKeyboardButton {
                text: text.to_owned(),
                url: None,
                callback_data: None,
                switch_inline_query: Some(switch_inline_query.to_owned()),
                switch_inline_query_current_chat: None,
                callback_game: None,
                pay: None,
            }
        }

        /// Constructor for a switch_inline_current button
        ///
        /// `text`: Label text of the button
        ///
        /// `switch_inline_query_current_chat`: If set, pressing the button will insert the bot‘s
        /// username and the specified inline query in the current chat's input field. Can be empty, in
        /// which case only the bot’s username will be inserted.
        pub fn new_switch_inline_current(
            text: &str,
            switch_inline_query_current_chat: &str,
        ) -> Self {
            InlineKeyboardButton {
                text: text.to_owned(),
                url: None,
                callback_data: None,
                switch_inline_query: None,
                switch_inline_query_current_chat: Some(switch_inline_query_current_chat.to_owned()),
                callback_game: None,
                pay: None,
            }
        }

        /// Constructor for a game button
        /// **NOTE**: This type of button **must** always be the first button in the first row.
        ///
        /// `text`: Label text of the button
        ///
        /// `callback_game`: Description of the game that will be launched when the user presses the button.
        pub fn new_game(text: &str, callback_game: CallbackGame) -> Self {
            InlineKeyboardButton {
                text: text.to_owned(),
                url: None,
                callback_data: None,
                switch_inline_query: None,
                switch_inline_query_current_chat: None,
                callback_game: Some(Box::new(callback_game)),
                pay: None,
            }
        }

        /// Constructor for a pay button
        /// **NOTE**: This type of button **must** always be the first button in the first row.
        ///
        /// `text`: Label text of the button
        pub fn new_pay(text: &str) -> Self {
            InlineKeyboardButton {
                text: text.to_owned(),
                url: None,
                callback_data: None,
                switch_inline_query: None,
                switch_inline_query_current_chat: None,
                callback_game: None,
                pay: Some(true),
            }
        }
    }
}

mod inline_query {
    use serde::{Deserialize, Serialize};

    use super::{Location, TelegramApiError, User};

    #[derive(Serialize, Deserialize, Debug)]
    pub struct InlineQuery {
        id: String,
        from: Box<User>,
        location: Option<Box<Location>>,
        query: String,
        offset: String,
    }

    /// Field getters
    impl<'a> InlineQuery {
        pub fn id(&self) -> String {
            self.id.clone()
        }

        pub fn from(&'a self) -> &'a User {
            self.from.as_ref()
        }

        pub fn location(&'a self) -> Result<&'a Location, TelegramApiError> {
            self.location
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("location", "InlineQuery"))
        }

        pub fn query(&self) -> String {
            self.query.clone()
        }

        pub fn offset(&self) -> String {
            self.offset.clone()
        }
    }
}

mod chosen_inline_result {
    use serde::{Deserialize, Serialize};

    use super::{Location, TelegramApiError, User};

    #[derive(Serialize, Deserialize, Debug)]
    pub struct ChosenInlineResult {
        result_id: i32,
        from: Box<User>,
        location: Option<Box<Location>>,
        inline_message_id: Option<String>,
        query: Option<String>,
    }

    impl<'a> ChosenInlineResult {
        pub fn id(&self) -> i32 {
            self.result_id
        }

        pub fn from(&'a self) -> &'a User {
            self.from.as_ref()
        }

        pub fn location(&'a self) -> Result<&'a Location, TelegramApiError> {
            self.location
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("location", "ChosenInlineResult"))
        }

        pub fn inline_message_id(&self) -> Result<String, TelegramApiError> {
            self.inline_message_id
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("inline_message_id", "ChosenInlineResult"))
        }

        pub fn query(&self) -> Result<String, TelegramApiError> {
            self.query
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("query", "ChosenInlineResult"))
        }
    }
}

mod inline_query_result {
    use serde::{Deserialize, Serialize};

    use super::{InlineKeyboardMarkup, InputMessageContent, TelegramApiError};

    /// The result of an inline query
    ///
    /// - `InlineQueryResultCachedAudio`
    /// - `InlineQueryResultCachedDocument`
    /// - `InlineQueryResultCachedGif`
    /// - `InlineQueryResultCachedMpeg4Gif`
    /// - `InlineQueryResultCachedPhoto`
    /// - `InlineQueryResultCachedSticker`
    /// - `InlineQueryResultCachedVideo`
    /// - `InlineQueryResultCachedVoice`
    /// - `InlineQueryResultArticle`
    /// - `InlineQueryResultAudio`
    /// - `InlineQueryResultContact`
    /// - `InlineQueryResultGame`
    /// - `InlineQueryResultDocument`
    /// - `InlineQueryResultGif`
    /// - `InlineQueryResultLocation`
    /// - `InlineQueryResultMpeg4Gif`
    /// - `InlineQueryResultPhoto`
    /// - `InlineQueryResultVenue`
    /// - `InlineQueryResultVideo`
    /// - `InlineQueryResultVoice`
    #[derive(Serialize, Deserialize, Debug)]
    pub struct InlineQueryResult {
        #[serde(rename = "type")]
        typ: String,
        id: String,
        title: Option<String>,
        input_message_content: Option<Box<InputMessageContent>>,
        reply_markup: Option<Box<InlineKeyboardMarkup>>,
        url: Option<String>,
        hide_url: Option<bool>,
        description: Option<String>,
        thumb_url: Option<String>,
        thumb_width: Option<i32>,
        thumb_height: Option<i32>,
        photo_url: Option<String>,
        photo_width: Option<i32>,
        photo_height: Option<i32>,
        caption: Option<String>,
        gif_url: Option<String>,
        gif_width: Option<i32>,
        gif_height: Option<i32>,
        gif_duration: Option<i32>,
    }

    impl Default for InlineQueryResult {
        fn default() -> Self {
            Self::InlineQueryResultArticle(
                "",
                "",
                Default::default(),
                None,
                None,
                false,
                None,
                None,
                None,
                None,
            )
        }
    }

    /// Field Getters
    impl<'a> InlineQueryResult {
        pub fn typ(&self) -> String {
            self.typ.clone()
        }

        pub fn id(&self) -> String {
            self.id.clone()
        }

        pub fn title(&self) -> Result<String, TelegramApiError> {
            self.title
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("title", "InlineQueryResult"))
        }

        pub fn input_message_content(
            &'a self,
        ) -> Result<&'a InputMessageContent, TelegramApiError> {
            self.input_message_content
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| {
                    TelegramApiError::field("input_message_content", "InlineQueryResult")
                })
        }

        pub fn reply_markup(&'a self) -> Result<&'a InlineKeyboardMarkup, TelegramApiError> {
            self.reply_markup
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("reply_markup", "InlineQueryResult"))
        }

        pub fn url(&self) -> Result<String, TelegramApiError> {
            self.url
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("url", "InlineQueryResult"))
        }

        pub fn hide_url(&self) -> Result<bool, TelegramApiError> {
            self.hide_url
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("hide_url", "InlineQueryResult"))
        }

        pub fn description(&self) -> Result<String, TelegramApiError> {
            self.description
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("description", "InlineQueryResult"))
        }

        pub fn thumb_url(&self) -> Result<String, TelegramApiError> {
            self.thumb_url
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("thumb_url", "InlineQueryResult"))
        }

        pub fn thumb_width(&self) -> Result<i32, TelegramApiError> {
            self.thumb_width
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("thumb_width", "InlineQueryResult"))
        }

        pub fn thumb_height(&self) -> Result<i32, TelegramApiError> {
            self.thumb_height
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("thumb_height", "InlineQueryResult"))
        }

        pub fn photo_url(&self) -> Result<String, TelegramApiError> {
            self.photo_url
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("photo_url", "InlineQueryResult"))
        }

        pub fn photo_width(&self) -> Result<i32, TelegramApiError> {
            self.photo_width
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("photo_width", "InlineQueryResult"))
        }

        pub fn photo_height(&self) -> Result<i32, TelegramApiError> {
            self.photo_height
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("photo_height", "InlineQueryResult"))
        }

        pub fn caption(&self) -> Result<String, TelegramApiError> {
            self.caption
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("caption", "InlineQueryResult"))
        }

        pub fn gif_url(&self) -> Result<String, TelegramApiError> {
            self.gif_url
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("gif_url", "InlineQueryResult"))
        }

        pub fn gif_width(&self) -> Result<i32, TelegramApiError> {
            self.gif_width
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("gif_width", "InlineQueryResult"))
        }

        pub fn gif_height(&self) -> Result<i32, TelegramApiError> {
            self.gif_height
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("gif_height", "InlineQueryResult"))
        }

        pub fn gif_duration(&self) -> Result<i32, TelegramApiError> {
            self.gif_duration
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("gif_duration", "InlineQueryResult"))
        }
    }

    /// Constructors
    #[allow(non_snake_case)]
    impl InlineQueryResult {
        pub fn InlineQueryResultArticle(
            id: &str,
            title: &str,
            input_message_content: InputMessageContent,
            reply_markup: Option<InlineKeyboardMarkup>,
            url: Option<&str>,
            hide_url: bool,
            description: Option<&str>,
            thumb_url: Option<&str>,
            thumb_width: Option<i32>,
            thumb_height: Option<i32>,
        ) -> Self {
            InlineQueryResult {
                typ: "article".to_owned(),
                id: id.to_owned(),
                title: Some(title.to_owned()),
                input_message_content: Some(Box::new(input_message_content)),
                reply_markup: reply_markup.map(Box::new),
                url: url.map(|s| s.to_owned()),
                hide_url: if hide_url { Some(true) } else { None },
                description: description.map(|s| s.to_owned()),
                thumb_url: thumb_url.map(|s| s.to_owned()),
                thumb_width,
                thumb_height,
                photo_url: None,
                photo_width: None,
                photo_height: None,
                caption: None,
                gif_url: None,
                gif_width: None,
                gif_height: None,
                gif_duration: None,
            }
        }

        pub fn InlineQueryResultPhoto(
            id: &str,
            photo_url: &str,
            thumb_url: &str,
            photo_width: Option<i32>,
            photo_height: Option<i32>,
            title: Option<&str>,
            description: Option<&str>,
            caption: Option<&str>,
            reply_markup: Option<InlineKeyboardMarkup>,
            input_message_content: Option<InputMessageContent>,
        ) -> Self {
            InlineQueryResult {
                typ: "photo".to_owned(),
                id: id.to_owned(),
                photo_url: Some(photo_url.to_owned()),
                thumb_url: Some(thumb_url.to_owned()),
                photo_width,
                photo_height,
                title: title.map(|s| s.to_owned()),
                description: description.map(|s| s.to_owned()),
                caption: caption.map(|s| s.to_owned()),
                reply_markup: reply_markup.map(Box::new),
                input_message_content: input_message_content.map(Box::new),
                hide_url: None,
                thumb_height: None,
                thumb_width: None,
                url: None,
                gif_url: None,
                gif_width: None,
                gif_height: None,
                gif_duration: None,
            }
        }

        pub fn InlineQueryResultGif(
            id: &str,
            gif_url: &str,
            gif_width: Option<i32>,
            gif_height: Option<i32>,
            gif_duration: Option<i32>,
            thumb_url: &str,
            title: Option<&str>,
            caption: Option<&str>,
            reply_markup: Option<InlineKeyboardMarkup>,
            input_message_content: Option<InputMessageContent>,
        ) -> Self {
            InlineQueryResult {
                typ: "gif".to_owned(),
                id: id.to_owned(),
                gif_url: Some(gif_url.to_owned()),
                gif_width,
                gif_height,
                gif_duration,
                thumb_url: Some(thumb_url.to_owned()),
                title: title.map(|s| s.to_owned()),
                caption: caption.map(|s| s.to_owned()),
                reply_markup: reply_markup.map(Box::new),
                input_message_content: input_message_content.map(Box::new),
                hide_url: None,
                thumb_height: None,
                thumb_width: None,
                url: None,
                photo_url: None,
                photo_width: None,
                photo_height: None,
                description: None,
            }
        }
    }
}

mod input_message_content {
    use serde::{Deserialize, Serialize};

    /// `InputTextMessageContent`
    /// `InputLocationMessageContent`
    /// `InputVenueMessageContent`
    /// `InputContactMessageContent`
    ///
    /// https://core.telegram.org/bots/api#inputmessagecontent
    #[derive(Serialize, Deserialize, Debug, Default)]
    pub struct InputMessageContent {
        message_text: Option<String>,
        parse_mode: Option<String>,
        disable_web_page_preview: Option<bool>,
        latitude: Option<f64>,
        longitude: Option<f64>,
        live_period: Option<i32>,
        title: Option<String>,
        address: Option<String>,
        foursquare_id: Option<String>,
        foursquare_type: Option<String>,
        phone_number: Option<String>,
        first_name: Option<String>,
        last_name: Option<String>,
        vcard: Option<String>,
    }

    /// Constructors
    #[allow(non_snake_case)]
    impl InputMessageContent {
        /// https://core.telegram.org/bots/api#inputtextmessagecontent
        pub fn InputTextMessageContent(
            message_text: String,
            parse_mode: Option<String>,
            disable_web_page_preview: Option<bool>,
        ) -> InputMessageContent {
            InputMessageContent {
                message_text: Some(message_text),
                parse_mode,
                disable_web_page_preview,
                ..Default::default()
            }
        }

        /// https://core.telegram.org/bots/api#inputlocationmessagecontent
        pub fn InputLocationMessageContent(
            latitude: f64,
            longitude: f64,
            live_period: Option<i32>,
        ) -> InputMessageContent {
            InputMessageContent {
                latitude: Some(latitude),
                longitude: Some(longitude),
                live_period,
                ..Default::default()
            }
        }

        /// https://core.telegram.org/bots/api#inputvenuemessagecontent
        pub fn InputVenueMessageContent(
            latitude: f64,
            longitude: f64,
            title: String,
            address: String,
            foursquare_id: Option<String>,
            foursquare_type: Option<String>,
        ) -> InputMessageContent {
            InputMessageContent {
                latitude: Some(latitude),
                longitude: Some(longitude),
                title: Some(title),
                address: Some(address),
                foursquare_id,
                foursquare_type,
                ..Default::default()
            }
        }

        /// https://core.telegram.org/bots/api#inputcontactmessagecontent
        pub fn InputContactMessageContent(
            phone_number: String,
            first_name: String,
            last_name: Option<String>,
            vcard: Option<String>,
        ) -> InputMessageContent {
            InputMessageContent {
                phone_number: Some(phone_number),
                first_name: Some(first_name),
                last_name,
                vcard,
                ..Default::default()
            }
        }
    }
}

mod callback_query {
    use serde::{Deserialize, Serialize};

    use super::{Message, TelegramApiError, User};

    #[derive(Serialize, Deserialize, Debug)]
    pub struct CallbackQuery {
        id: String,
        from: Box<User>,
        message: Option<Box<Message>>,
        inline_message_id: Option<String>,
        chat_instance: Option<String>,
        data: Option<String>,
        game_short_name: Option<String>,
    }

    impl<'a> CallbackQuery {
        pub fn id(&self) -> String {
            self.id.clone()
        }

        pub fn from(&'a self) -> &'a User {
            self.from.as_ref()
        }

        pub fn message(&'a self) -> Result<&'a Message, TelegramApiError> {
            self.message
                .as_ref()
                .map(|m| m.as_ref())
                .ok_or_else(|| TelegramApiError::field("message", "CallbackQuery"))
        }

        pub fn inline_message_id(&self) -> Result<String, TelegramApiError> {
            self.inline_message_id
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("inline_message_id", "CallbackQuery"))
        }

        pub fn chat_instance(&self) -> Result<String, TelegramApiError> {
            self.chat_instance
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("chat_instance", "CallbackQuery"))
        }

        pub fn data(&self) -> Result<String, TelegramApiError> {
            self.data
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("data", "CallbackQuery"))
        }

        pub fn game_short_name(&self) -> Result<String, TelegramApiError> {
            self.game_short_name
                .as_ref()
                .cloned()
                .ok_or_else(|| TelegramApiError::field("game_short_name", "CallbackQuery"))
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ForceReply {
    /// Must be set to True
    force_reply: bool,
    selective: Option<bool>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ChatPhoto {
    small_file_id: String,
    big_file_id: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ChatMember {
    user: Box<User>,
    status: String,
    until_date: Option<i32>,
    can_be_edited: Option<bool>,
    can_change_info: Option<bool>,
    can_post_messages: Option<bool>,
    can_edit_messages: Option<bool>,
    can_delete_messages: Option<bool>,
    can_invite_users: Option<bool>,
    can_restrict_members: Option<bool>,
    can_pin_messages: Option<bool>,
    can_promote_members: Option<bool>,
    can_send_messages: Option<bool>,
    can_send_media_messages: Option<bool>,
    can_send_other_messages: Option<bool>,
    can_add_web_page_previews: Option<bool>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ResponseParameters {
    migrate_to_chat_id: Option<i32>,
    retry_after: Option<i32>,
}

/// `InputMediaPhoto`
/// `InputMediaVideo`
#[derive(Serialize, Deserialize, Debug)]
pub struct InputMedia {
    /// `photo` or `video`
    #[serde(rename = "type")]
    typ: String,
    media: String,
    caption: Option<String>,
    width: Option<i32>,
    height: Option<i32>,
    duration: Option<i32>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Sticker {
    file_id: String,
    width: i32,
    height: i32,
    thumb: Option<Box<PhotoSize>>,
    emoji: Option<String>,
    set_name: Option<String>,
    mask_position: Option<MaskPosition>,
    file_size: Option<i32>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct StickerSet {
    name: String,
    title: String,
    contains_masks: bool,
    stickers: Vec<Box<Sticker>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MaskPosition {
    point: String,
    x_shift: f64,
    y_shift: f64,
    scale: f64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Game {
    title: String,
    description: String,
    photo: Vec<Box<PhotoSize>>,
    text: Option<String>,
    text_entities: Vec<Box<MessageEntity>>,
    animation: Option<Box<Animation>>,
}

#[derive(Serialize, Deserialize, Debug)]
#[cfg_attr(test, derive(PartialEq))]
pub struct CallbackGame {
    // Currently empty see API documentation
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LabeledPrice {
    label: String,
    amount: i32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Invoice {
    title: String,
    description: String,
    start_parameter: String,
    currency: String,
    total_amount: i32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ShippingAddress {
    country_code: String,
    state: String,
    city: String,
    street_line1: String,
    street_line2: String,
    post_code: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct OrderInfo {
    name: Option<String>,
    phone_number: Option<String>,
    email: Option<String>,
    shipping_address: Option<Box<ShippingAddress>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ShippingOption {
    id: String,
    title: String,
    prices: Vec<Box<LabeledPrice>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SuccessfulPayment {
    currency: String,
    total_amount: i32,
    invoice_payload: String,
    shipping_option_id: String,
    order_info: Option<Box<OrderInfo>>,
    telegram_payment_charge_id: String,
    provider_payment_charge_id: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ShippingQuery {
    id: String,
    from: Box<User>,
    invoice_payload: String,
    shipping_address: Box<ShippingAddress>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PreCheckoutQuery {
    id: String,
    from: Box<User>,
    currency: String,
    total_amount: i32,
    invoice_payload: String,
    shipping_option_id: Option<String>,
    order_info: Option<Box<OrderInfo>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PassportData {
    data: Vec<Box<EncryptedPassportElement>>,
    credentials: Box<EncryptedCredentials>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PassportFile {
    file_id: String,
    file_size: i32,
    file_date: i32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EncryptedPassportElement {
    #[serde(rename = "type")]
    typ: String,
    data: Option<String>,
    phone_number: Option<String>,
    email: Option<String>,
    files: Option<Vec<Box<PassportFile>>>,
    front_side: Option<PassportFile>,
    reverse_side: Option<PassportFile>,
    selfie: Option<PassportFile>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EncryptedCredentials {
    data: String,
    hash: String,
    secret: String,
}

// Result
#[derive(Serialize, Deserialize, Debug)]
pub struct ApiResponse<T> {
    pub(crate) ok: bool,
    pub(crate) result: Option<T>,
    pub(crate) description: Option<String>,
    pub(crate) error_code: Option<i32>,
    pub(crate) parameters: Option<ResponseParameters>,
}

impl<T> ApiResponse<T> {
    pub fn into_result(self) -> Result<T, TelegramApiError> {
        if self.ok {
            Ok(self.result.unwrap())
        } else {
            Err(TelegramApiError::from(self))
        }
    }
}

// ----------------------------------------------------------------------------

/// Marker trait for different `replay_markup`s
/// [`/sendMessage`](../methods/struct.SendMessage.html) requests
///
/// - [`InlineKeyboardMarkup`](struct.InlineKeyboardMarkup.html)
/// - [`ReplyKeyboardMarkup`](struct.ReplyKeyboardMarkup.html)
/// - [`ReplyKeyboardRemove`](struct.ReplyKeyboardRemove.html)
/// - [`ForceReply`](struct.ForceReply.html)
pub trait ReplayMarkup: Serialize + Debug {}

#[doc(hidden)]
impl ReplayMarkup for () {}
impl ReplayMarkup for InlineKeyboardMarkup {}
impl ReplayMarkup for ReplyKeyboardMarkup {}
impl ReplayMarkup for ReplyKeyboardRemove {}
impl ReplayMarkup for ForceReply {}

// ----------------------------------------------------------------------------

/// This Response is returned from:
///
/// - [`/getMe` API](https://core.telegram.org/bots/api#getme)
pub type UserResponse = ApiResponse<User>;

/// This Response is returned from:
///
/// - [`/getUpdates`](../methods/struct.GetUpdates.html)
pub type UpdatesResponse = ApiResponse<Vec<Update>>;

/// This Response is returned from:
///
/// - [`/sendMessage`](../methods/struct.SendMessage.html)
/// - [`/editMessageText`](../methods/struct.EditMessageText.html)
/// - TODO
pub type MessageResponse = ApiResponse<Message>;

/// This Response is returned from:
///
/// - [`/deleteMessage`](../methods/struct.DeleteMessage.html)
pub type BoolResponse = ApiResponse<bool>;
