#!/bin/bash

set -e

cargo fmt
cargo build $1
cargo doc --no-deps $1

echo "Done."